package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
)

func main() {
	sampleCode, _ := getFile("program.txt")
	input, _ := getFile("testcase/input.txt")
	output, _ := getFile("testcase/output.txt")

	writeMainPython(sampleCode)

  cmd := exec.Command("python3", "tmp/main.py")
  stdin, _ := cmd.StdinPipe()
  io.WriteString(stdin, input)
  stdin.Close()
  out, err := cmd.Output()
	if err != nil {
		fmt.Println(err)
	}
  fmt.Printf("result: %s %s", output, string(out))
	if string(out) == output {
		println("Accepted!!!")
	} else {
		println("Failed!!!")
	}
}

func getFile(fileName string) (string, error) {
	name := fmt.Sprintf("tmp/%s", fileName)
	f, err := os.Open(name)
	if err != nil {
		return "", err
	}

	data := make([]byte, 1024)
	c, err := f.Read(data)
	if err != nil {
		return "", err
	}

	return string(data[:c]), nil
}

func writeMainPython(code string) {
	f, err := os.Create("tmp/main.py")
	if err != nil {
		fmt.Println(err)
	}

	data := []byte(code)
	_, err = f.Write(data)
	if err != nil {
		fmt.Println(err)
	}

	defer func() {
		err := f.Close()
		if err != nil {
			fmt.Println(err)
		}
	}()
}
